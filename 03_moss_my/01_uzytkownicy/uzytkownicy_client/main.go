package main

import (
	"context"
	"fmt"
	"log"
	"time"

	pb "gitlab.com/marek22222/goProj/gRCP/03_moss_my/01_uzytkownicy/uzytkownicy"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("brak połączenia: %v", err)
	}
	defer conn.Close()
	c := pb.NewZarzadzanieClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	var nowy_uzytkownik = make(map[string]int32)
	nowy_uzytkownik["Alice"] = 43
	nowy_uzytkownik["Bob"] = 33
	for nazwa, wiek := range nowy_uzytkownik {
		r, err := c.UtworzUzytkownika(ctx, &pb.NowyUzytkownik{
			Nazwa: nazwa,
			Wiek:  wiek,
		})
		if err != nil {
			log.Fatalf("nie można utworzyć użytkownika: %v", err)
		}
		log.Printf("Szczegóły uzytkownika: nazwa: %s, wiek: %d, id: %d",
			r.GetNazwa(), r.GetWiek(), r.GetId())
	}
	params := &pb.PobierzPrmUzytkownikow{}
	r, err := c.PobierzUzytkownikow(ctx, params)
	if err != nil {
		log.Fatalf("nie można otrzymać listy uzytkownikóów")
	}
	log.Print("\nLista Użytkowników: \n")
	fmt.Printf("r.GetUzytkownicy(): %v", r.GetUzytkownicy())
}
