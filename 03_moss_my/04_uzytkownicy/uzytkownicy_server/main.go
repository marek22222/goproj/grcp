package main

import (
	"context"
	"log"
	"math/rand"
	"net"

	pb "gitlab.com/marek22222/goProj/grcp/03_moss_my/04_uzytkownicy/uzytkownicy"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

type ZarzadzanieServer struct {
	pb.UnimplementedZarzadzanieServer
}

func (s *ZarzadzanieServer) UtworzNowegoUsera(
	kxt context.Context, in *pb.NowyUser) (*pb.User, error) {

	log.Printf("Otrzymano: %v", in.GetNazwa())
	var user_id int32 = int32(rand.Intn(1000))
	return &pb.User{
		Nazwa: in.GetNazwa(),
		Wiek:  in.GetWiek(),
		Id:    user_id,
	}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterZarzadzanieServer(s, &ZarzadzanieServer{})
	log.Printf("serwer nasłuchuje na: %v", lis.Addr())
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("bład serwera: %v", err)
	}
}
